WPClock
=======

Copyright © 2009, 2013 Espen Rønnevik

about
-----

WPClock is a program that works with Wallpaper Clocks. These are special wallpapers
that are generated and incorporates other information into the design, usually
time and date. WPClock also supports creating wallpapers with zodiac signs and moonphase
information in them.

Wallpaperclocks to use with this program can be downloaded from [vladstudio.com](http://www.vladstudio.com/wallpaperclocks/developers_specs.php)
or you can look [here](http://www.vladstudio.com/wallpaperclock_create/) for information on how to create your own.

license information
-------------------

WPClock is distributed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.  A copy of this license
can be found in the file COPYING included with the source code of this
program.


dependencies
------------

The following software must be installed for WPClock to function correctly:

* Python 3
* PyQt4
