#
#       wcz.py
#
#       Copyright 2009 Espen Rønnevik <espen@ronnevik.net>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

"""
wcz - Manipulation of WallpaperClock files.

This module allows you to load wcz-files and generate wallpapers
from them.

WCZ specs can be found here:
    http://www.vladstudio.com/wallpaperclocks/developers_specs.php

Usage overview:
  Make an instance of the class, passing in the path for the wcz file
  you'd like to use and use a combination of the following two methods:

  properties:
    fout - Specify the filename of the written file.

  generate(unixtimestamp):
    This function will generate a new wallpaper from the given timestamp
    and store it in a buffer.

  Then call generate() with a unix timestamp to make
  a new wallpaper image using the timestamp information and buffer it.
  Finally call write() with a path which will write the buffered wallpaper
  to a file. Optionally call write() directly
"""


import os
from time import localtime, mktime
from zipfile import ZipFile
from configparser import ConfigParser
from collections import OrderedDict

from PyQt4.QtGui import QImage, QPixmap

from moon import Moon


class WczFile(object):

    def __init__(self, wczfile):

        with ZipFile(wczfile, 'r') as wcz:

            zipmembers = wcz.namelist()

            # Read the clock configuration file
            self._config = ConfigParser()
            configfile = wcz.open('clock.ini', 'rU').read()
            self._config.read_string(configfile.decode('us-ascii'), source='clock.ini')
            zipmembers.remove('clock.ini')

            # Read the background image
            self._images = {'background': self._loadImage('bg.jpg', wcz)}
            zipmembers.remove('bg.jpg')

            # Read the preview image as raw data, ignore the small optional one
            self._images['preview'] = wcz.open('preview200x150.jpg').read()
            zipmembers.remove('preview200x150.jpg')
            try:
                zipmembers.remove('preview100x75.jpg')
            except ValueError:
                pass

            # Read in the rest of the images in the file as raw data
            for member in zipmembers:
                name, ext = os.path.splitext(member)
                self._images[name] = wcz.open(member).read()

        if self.refreshHourInterval == 60:
            self._clockmode = self.hourImages
        else:
            self._clockmode = (self.hourImages * self.refreshHourInterval) // 60

        self._moon = Moon()

    def _loadImage(self, imagedata, archive=None):
        if imagedata is not None:
            if archive is not None:
                imagedata = archive.open(imagedata).read()
            image = QImage.fromData(imagedata)
        else:
            image = None

        return image

    @property
    def name(self):
        return self._config.get('Settings', 'name')

    @property
    def width(self):
        return self._config.getint('Settings', 'width')

    @property
    def height(self):
        return self._config.getint('Settings', 'height')

    @property
    def author(self):
        return self._config.get('Settings', 'author')

    @property
    def email(self):
        return self._config.get('Settings', 'email')

    @property
    def ampmEnabled(self):
        return self._config.getboolean('Settings', 'ampmenabled')

    @property
    def refreshHourInterval(self):
        return self._config.getint('Settings', 'refreshhourinterval')

    @property
    def hourImages(self):
        return self._config.getint('Settings', 'hourimages')

    @property
    def clockMode(self):
        return self._clockmode

    @property
    def background(self):
        """ Return the background image """
        return self._images['background']

    @property
    def preview(self):
        """ Return preview image """
        preview = QPixmap()
        preview.loadFromData(self._images['preview'])
        return preview

    def getZodiacImage(self, tts):
        """ Returns the correct zodiac image """

        image = None

        # If we got one of them, assume they are all present
        if 'zodiacCapricorn' in self._images:
            md = tts.tm_mon * 100 + tts.tm_mday

            if 119 < md < 218:
                sign = 'zodiacAquarius'
            elif 217 < md < 320:
                sign = 'zodiacPisces'
            elif 319 < md < 419:
                sign = 'zodiacAries'
            elif 418 < md < 520:
                sign = 'zodiacTaurus'
            elif 519 < md < 621:
                sign = 'zodiacGemini'
            elif 620 < md < 722:
                sign = 'zodiacCancer'
            elif 721 < md < 823:
                sign = 'zodiacLeo'
            elif 822 < md < 922:
                sign = 'zodiacVirgo'
            elif 921 < md < 1023:
                sign = 'zodiacLibra'
            elif 1022 < md < 1122:
                sign = 'zodiacScorpio'
            elif 1121 < md < 1221:
                sign = 'zodiacSagittarius'
            else:
                sign = 'zodiacCapricorn'

            imagedata = self._images.get(sign, None)
            image = self._loadImage(imagedata)

        return image

    def getMoonphaseImage(self, tts):
        """ Returns a the correct moon phase image """

        image = None

        # Like zodiac, assume all are present if we can find one
        if 'moonphase1' in self._images:
            self._moon.update(mktime(tts))
            imagedata = self._images.get('moonphase' + str(self._moon.age + 1), None)
            image = self._loadImage(imagedata)

        return image

    def getMonthImage(self, tts):
        """ Returns the correct month image """
        image = self._images.get('month' + str(tts.tm_mon), None)
        return self._loadImage(image)

    def getWeekdayImage(self, tts):
        """ Returns the correct weekday image """
        image = self._images.get('weekday' + str(tts.tm_wday + 1), None)
        return self._loadImage(image)

    def getDayImage(self, tts):
        """ Returns the correct day image """
        image = self._images.get('day' + str(tts.tm_mday), None)
        return self._loadImage(image)

    def getHourImage(self, tts):
        """ Returns the correct hour image """

        # Calculating the hour image, taking into account the hourly refresh interval
        # and if it's a 12hour or 24hour clock.

        hour_offset = tts.tm_hour - (self.clockMode * (tts.tm_hour // self.clockMode))
        hour_offset *= 60 // self.refreshHourInterval
        min_offset = tts.tm_min // self.refreshHourInterval

        image = self._images.get('hour' + str(hour_offset + min_offset), None)
        return self._loadImage(image)

    def getMinuteImage(self, tts):
        """ Return the correct minute image """
        image = self._images.get('minute' + str(tts.tm_min), None)
        return self._loadImage(image)

    def getSecondImage(self, tts):
        """ Return the correct second image """
        image = self._images.get('second' + str(tts.tm_sec), None)
        return self._loadImage(image)

    def getAmPmImage(self, tts):
        """ Return the correct am/pm image """
        if self.ampmEnabled and self.clockMode == 12:
            if self.clockMode * (tts.tm_hour // self.clockMode):
                image = self._images.get('pm', None)
            else:
                image = self._images.get('am', None)
        else:
            image = None

        return self._loadImage(image)

    def getDayImages(self, tts, future=False):
        """ Return dictionary with images remaining constant for one day """

        members = {
            'zodiac': self.getZodiacImage,
            'moonphase': self.getMoonphaseImage,
            'month': self.getMonthImage,
            'weekday': self.getWeekdayImage,
            'day': self.getDayImage
        }

        t = tts

        if future:
            future_ts = mktime(tts)
            t = localtime(future_ts + (84600 - (future_ts % 84600)))

        images = OrderedDict()
        for name, fn in members.items():
            image = fn(t)
            if image is not None:
                images[name] = fn(t)

        t = localtime(min(mktime(t), self._moon.nextupdate))
        return (t, images)

    def getHourImages(self, tts, future=False):
        """ Return dictionary with images remaining constant for one hour """

        # Note: According to the specifications, am/pm image should be the last
        # image applied to the constructed image. However, this means processing
        # am/pm image more often than necessary. For efficiency's sake, I will
        # include it among the hourly generated images.

        members = {
            'hour': self.getHourImage,
            'ampm': self.getAmPmImage
        }

        t = tts

        if future:
            future_ts = mktime(tts)
            t = localtime(future_ts + (3600 - (future_ts % 3600)))

        images = OrderedDict()
        for name, fn in members.items():
            image = fn(t)
            if image is not None:
                images[name] = fn(t)

        return (t, images)

    def getImages(self, tts):
        images = OrderedDict()
        images.update(self.getDayImages(tts)[1])
        images.update(self.getHourImages(tts)[1])
        images['minute'] = self.getMinuteImage(tts)

        # TODO: Implement seconds, ignore it for now
        # images['second'] = self.getSecondImage(tts)

        return images

#-----------------------------------------------------------------------

if __name__ == '__main__':
    pass
