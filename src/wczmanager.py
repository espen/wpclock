#
#       wczmanager.py
#
#       Copyright 2009 Espen Rønnevik <espen@ronnevik.net>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import os
from glob import glob
from shutil import copy

from PyQt4.QtGui import QDialog, QFileDialog
from PyQt4.QtCore import Qt

from ui_wczmanager import Ui_wczManager
from wcz import WczFile


class wczManager(QDialog, Ui_wczManager):

    def __init__(self, config, parent=None, flags=0):
        QDialog.__init__(self, parent, Qt.WindowFlags(flags))
        self.setupUi(self)
        self.path = config.get('wpclock', 'path')

        self.wczList.currentRowChanged.connect(self._rowchange)
        self.installButton.clicked.connect(self._install)

        files = glob(os.path.join(self.path, '*.wcz'))
        self._wczFiles = [WczFile(f) for f in files]

        for wcz in self._wczFiles:
            self.wczList.addItem(wcz.name)

        self.wczList.setCurrentRow(0)

    @property
    def wcz(self):
        return self._currentWcz

    def _rowchange(self, row):
        self._currentWcz = self._wczFiles[row]
        self.authorLabel.setText(self._currentWcz.author)
        self.emailLabel.setText(self._currentWcz.email)
        self.sizeLabel.setText('{}x{}'.format(self._currentWcz.width, self._currentWcz.height))
        self.preview.setPixmap(self._currentWcz.preview)

    def _install(self):
        selectedFile = QFileDialog.getOpenFileName(filter='*.wcz')
        copy(selectedFile, self.path)
        path, filename = os.path.split(selectedFile)
        wczfile = os.path.join(self.path, filename)
        wcz = WczFile(wczfile)
        self._wczFiles.append(wcz)
        self.wczList.addItem(wcz.name)
