#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       wpclock.py
#
#       Copyright 2009, 2013 Espen Rønnevik <espen@ronnevik.net>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import os
import sys
from time import mktime, localtime
from configparser import ConfigParser
from queue import Queue, Empty
from functools import partial
from threading import Thread

from PyQt4 import QtCore, QtGui

from wczmanager import wczManager
from desktop import Desktop


class WPClock(QtGui.QSystemTrayIcon):

    def __init__(self, icon, parent=None):
        QtGui.QSystemTrayIcon.__init__(self, icon, parent)

        # Construct the menu
        self.menu = QtGui.QMenu(parent)

        # Todo: Add options and about
        self.menu.addAction('Manage wallpapers', self.manage)
        #self.menu.addAction('Options', self.options)
        #self.menu.addAction('About', self.about)

        self.menu.addSeparator()
        self.menu.addAction('Quit', self.quit)
        self.setContextMenu(self.menu)

        # Initialize the configuration
        self.config = ConfigParser()
        self.config.add_section('wpclock')
        homePath = os.path.expanduser('~')
        wpclockPath = os.path.join(homePath, 'wpclock')
        self.config.set('wpclock', 'path', wpclockPath)
        self.config.set('wpclock', 'useampm', 'no')

        if not os.path.exists(wpclockPath):
            os.mkdir(wpclockPath)

        self._desktop = None
        self._generatorActive = False
        self._generatorThread = None
        self._inputQueue = None
        self._imageQueue = None
        self._output = None

    @property
    def name(self):
        return self._wcz.name

    @property
    def path(self):
        return self.config.get('wpclock', 'path')

    @property
    def useAmPm(self):
        return self.config.getboolean('wpclock', 'useampm')

    def manage(self):
        dialog = wczManager(self.config)
        if dialog.exec():
            self._wcz = dialog.wcz
            self._desktop = Desktop(self, handler='xml')
            self._startImageGenerator()

    def options(self):
        pass

    def about(self):
        pass

    def _startImageGenerator(self):
        self._stopImageGenerator()

        # Initialize queues
        self._inputQueue = Queue(3)
        self._imageQueue = Queue(3)
        self._output = {}

        # Start image-generation thread
        self._generatorThread = Thread(target=self.imageGenerator)
        self._generatorThread.start()
        self._generatorActive = True

        current_ts = int(mktime(localtime()))
        min_delta = current_ts % 60
        ts = current_ts - min_delta
        self._inputQueue.put(ts)

        QtCore.QTimer().singleShot(0, self.imageScheduler)

    def _stopImageGenerator(self):
        if self._generatorActive:
            self._inputQueue.put(None)
            self._imageQueue.put((None, None))
            self._generatorThread.join()

    def quit(self):
        self._desktop.close()
        self._stopImageGenerator()
        QtGui.QApplication.quit()

    def imageGenerator(self):
        ts = self._inputQueue.get()

        while ts is not None:
            self._imageQueue.put((ts, self._generateImage(ts)))
            ts = self._inputQueue.get()

    def _generateImage(self, ts):
        images = self._wcz.getImages(localtime(ts))
        return self._createComposite(
            self._wcz.background,
            images.values()
        )

    def _createComposite(self, baseimage, overlays):
        composite = baseimage.copy(QtCore.QRect())
        painter = QtGui.QPainter()
        painter.begin(composite)
        painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
        for overlay in overlays:
            painter.drawImage(0, 0, overlay)
        painter.end()
        return composite

    def imageScheduler(self):
        try:
            ts, image = self._imageQueue.get(block=False)
            if ts is not None:
                self._output[ts] = image

                if self._desktop.punctual:
                    pickup_ts = int((ts - mktime(localtime())) * 1000)
                    if pickup_ts < 0:
                        pickup_ts = 0
                else:
                    pickup_ts = 0

                setfn = partial(self.imageDelivery, ts)
                QtCore.QTimer().singleShot(pickup_ts, setfn)
        except Empty:
            QtCore.QTimer().singleShot(1000, self.imageScheduler)

    def imageDelivery(self, ts):
        self._desktop.background = self._output.pop(ts)
        self._inputQueue.put(ts + 60)
        QtCore.QTimer().singleShot(0, self.imageScheduler)


def main():
    app = QtGui.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)

    wpclock = WPClock(QtGui.QIcon(':icons/icon_64x64.png'))
    wpclock.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()