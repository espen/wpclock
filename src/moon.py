#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       moon.py
#
#       Copyright 2009,2013 Espen Rønnevik <espen@ronnevik.net>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import sys
from math import sqrt, sin, cos, tan, atan, radians, degrees, fabs

class Moon(object):
    # Astronomical constants.

    unixepoch = 2440587.5       # Midnight 1 jan 1970
    astroepoch = 2444238.5      # Midnight 1 jan 1980
    epsilon = 1.0e-6            # Limiting value for Kepler equation

    # Constants defining the Sun's apparent orbit.

    s_elong = 278.833540        # ecliptic longitude of the Sun at epoch
    s_elongp = 282.596403       # ecliptic longitude of the Sun at perigee
    s_eccent = 0.016718         # eccentricity of Earth's orbit

    # Constants defining the Moon's orbit.

    m_mlong = 64.975464         # moon's mean longitude at the epoch
    m_mlongp = 349.383063       # mean longitude of the perigee at the epoch
    m_longnode = 151.950429     # mean longitude of the node at the epoch
    m_eccent = 0.054900         # eccentricity of the Moon's orbit
    m_synodic = 29.53058868     # synodic month (new Moon to new Moon)

    def __init__(self):
        self._ts = 0
        self._phasesage = 0
        self._illuminated = 0
        self._age = 0
        self._nextupdate = sys.maxsize

    @property
    def phase(self):
        """ Returns fraction of moon phase """
        return self._phase

    @property
    def illuminated(self):
        """ Returns fraction of moon illuminated """
        return self._illuminated

    @property
    def age(self):
        """ Returns moon age in days """
        return self._age

    @property
    def nextupdate(self):
        """ Returns unix timestamp of when moon has aged by one """
        return self._nextupdate

    def update(self, ts):
        if self._ts < ts:
            self._ts = ts
            if ts >= self.nextupdate:
                self._calculate()

    def fixangle(self, angle):
        return angle - 360 * (angle // 360)

    def kepler(self, m, eccent):
        e = m = radians(m)
        delta = 1

        while fabs(delta) > self.epsilon:
            delta = e - eccent * sin(e) - m
            e -= delta / (1 - eccent * cos(e))
        return e

    def _calculate(self):

        # Calculation of the Sun's position.
        # ----------------------------------

        # Modified Julian Date within astroepoch
        Day = (self.unixepoch + (self._ts / 86400)) - self.astroepoch

        # Mean anomaly of the Sun
        N = self.fixangle((360.0 / 365.2422) * Day)

        # Convert from perigee coordinates to epoch
        M = self.fixangle(N + self.s_elong - self.s_elongp)

        # Solve equation of Kepler
        Ec = self.kepler(M, self.s_eccent)

        # True anomaly in degrees
        Ec = sqrt((1 + self.s_eccent) / (1 - self.s_eccent)) * tan(Ec / 2)
        Ec = 2 * degrees(atan(Ec))

        # Sun's geocentric ecliptic longitude
        lsun = self.fixangle(Ec + self.s_elongp)

        # Calculation of the Moon's position
        # ----------------------------------

        # Moon's mean longitude
        ml = self.fixangle(13.1763966 * Day + self.m_mlong)

        # Moon's mean anomaly
        MM = self.fixangle(ml - 0.1114041 * Day - self.m_mlongp)

        # Ascending node mean anomaly

        # Research:
        # Commented out MN variable as it is not currently
        # in use. Should this be removed, or is this the correct
        # variable to use for calculating Evection?

        # MN = self.fixangle(self.m_longnode - 0.0529539 * Day)

        # Evection
        Ev = 1.2739 * sin(radians(2 * (ml - lsun) - MM))

        # Annual equation.
        Ae = 0.1858 * sin(radians(M))

        # Correction term.
        A3 = 0.37 * sin(radians(M))

        # Corrected anomaly.
        MmP = MM + Ev - Ae - A3

        # Correction for the equation of the centre.
        mEc = 6.2886 * sin(radians(MmP))

        # Another correction term.
        A4 = 0.214 * sin(radians(2 * MmP))

        # Corrected longitude.
        lP = ml + Ev + mEc - Ae + A4

        # Variation.
        V = 0.6583 * sin(radians(2 * (lP - lsun)))

        # True longitude.
        lPP = lP + V

        # Calculation of the phase of the Moon.
        # -------------------------------------

        # Age of the Moon in degrees.
        agedegrees = lPP - lsun

        # Fraction of moon phase
        phase = radians(self.fixangle(agedegrees))

        # Fraction of moon illuminated.
        illuminated = (1 - cos(radians(agedegrees))) / 2

        # Moon age in days
        age = self.m_synodic * (self.fixangle(agedegrees) / 360.0)

        # Calculate Unix timestamp for next moon phase change
        if int(age) == 29:
            remainder = self.m_synodic % 1 - age % 1
        else:
            remainder = 1 - age % 1

        phasehours = int(24 * remainder)
        phasemins = int(1440 * remainder) % 60
        phasesecs = int(86400 * remainder) % 60

        changesecs = phasehours * 3600 + phasemins * 60 + phasesecs
        next = self._ts + changesecs

        # Populate instance with calculated phase values
        self._phase = phase
        self._illuminated = illuminated
        self._age = age
        self._nextupdate = next

#-----------------------------------------------------------------------

if __name__ == '__main__':
    pass
