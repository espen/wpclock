#
#       desktop.py
#
#       Copyright 2009 Espen Rønnevik <espen@ronnevik.net>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import os
import platform
import subprocess

from PyQt4.QtGui import QWidget, QPainter, QPixmap, QApplication
from PyQt4.QtCore import Qt


class QtHandler(QWidget):

    def __init__(self, wpclock, size):

        # Todo: Figure out a way to set this window so it overlays the true background

        flags = (
            Qt.WindowStaysOnBottomHint |
            Qt.SplashScreen
        )

        QWidget.__init__(self, None, flags)

        self.setAttribute(Qt.WA_OpaquePaintEvent)
        self.setGeometry(size)
        self.move(0, 0)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.drawPixmap(0, 0, self._image)
        if self._image.width() < self.width():
            painter.drawPixmap(self._image.width(), 0, self._image)
        painter.end()

    @property
    def background(self):
        return self._image

    @background.setter
    def background(self, image):
        self._image = QPixmap.fromImage(image)
        self.update()

        if self.isHidden():
            self.show()

    def restore_background(self):
        self.close()

    @property
    def punctual(self):
        return True


class GSettingsHandler(object):

    def __init__(self, wpclock):
        self._oldBackground = self.background
        self._filename = os.path.join(wpclock.path, wpclock.name) + '.jpg'

    def _getBackground(self):
        cmd = ['gsettings',
               'get',
               'org.gnome.desktop.background',
               'picture-uri']
        return subprocess.check_output(cmd, universal_newlines=True).strip("'\n ")

    def _setBackground(self, filename):
        uri = 'file://' + filename
        cmd = ['gsettings',
               'set',
               'org.gnome.desktop.background',
               'picture-uri',
               uri
        ]
        subprocess.call(cmd)

    @property
    def background(self):
        return self._getBackground()[7:]

    @background.setter
    def background(self, image):
        if image is not None:
            filename = self.filename
            image.save(filename)
        else:
            filename = self._oldBackground

        self._setBackground(filename)

    def restore_background(self):
            self.background = None

    @property
    def punctual(self):
        return True

    @property
    def filename(self):
        return self._filename


class BaseDesktop(object):

    def __init__(self):
        super().__init__()
        self._desktop = QApplication.desktop()
        self._handler = None

    @property
    def desktop(self):
        return self._desktop

    @property
    def handler(self):
        if self._handler is None:
            raise AttributeError('Desktop has no handler')
        return self._handler

    @property
    def background(self):
        return self.handler.background

    @background.setter
    def background(self, image):
        self.handler.background = image

    @property
    def punctual(self):
        return self.handler.punctual

    def close(self):
        self.handler.restore_background()
        self._desktop = None


class GnomeDesktop(BaseDesktop):

    def __init__(self, wpclock, handler=None):
        super().__init__()

        if handler == 'pixmap':
            screen = self.desktop.screen()
            imagehandler = QtHandler(wpclock, screen.geometry())
        elif handler == 'gsettings':
            imagehandler = GSettingsHandler(wpclock)
        else:
            # Default handler
            imagehandler = GSettingsHandler(wpclock)

        self._handler = imagehandler


system = platform.system()
Desktop = None

if system == 'Linux':
    session = os.getenv('DESKTOP_SESSION')
    if session == 'gnome':
        Desktop = GnomeDesktop
